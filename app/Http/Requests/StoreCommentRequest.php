<?php

namespace App\Http\Requests;

use App\Exceptions\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return request()->user()->tokenCan('create-comment');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'content' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'content.required' => 'Komen tidak boleh kosong'
        ];
    }

    protected function failedAuthorization()
    {
        throw new AuthorizationException('Unauthorized', 403);
    }
}
