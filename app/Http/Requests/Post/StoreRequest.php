<?php

namespace App\Http\Requests\Post;

use App\Exceptions\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return request()->user()->tokenCan('create-post');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|image|max:5000'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Judul tidak boleh kosong',
            'content.required' => 'Konten tidak boleh kosong',
            'image.required' => 'Gambar tidak boleh kosong',
            'image.image' => 'File harus berupa gambar',
            'image.max' => 'File tidak boleh lebih dari :max'
        ];
    }

    protected function failedAuthorization()
    {
        throw new AuthorizationException('Unauthorized', 403);
    }
}
