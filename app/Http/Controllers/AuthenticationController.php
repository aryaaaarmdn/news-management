<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authentication\LoginRequest;
use App\Http\Requests\Authentication\RegisterRequest;
use App\Services\Authentication\LoginService;
use App\Services\Authentication\LogoutService;
use App\Services\Authentication\RegisterService;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    private $service;
    private static $mapping = [
        'login' => LoginService::class,
        'register' => RegisterService::class,
        'logout' => LogoutService::class
    ];

    public function __construct()
    {
        $this->middleware(function($request, $next) {
            $routeName = $request->route()->getName();
            $this->service = new self::$mapping[$routeName]();
            return $next($request);
        });
    }

    public function register(RegisterRequest $request)
    {
        return $this->service->process($request);
    }

    public function login(LoginRequest $request)
    {
        return $this->service->process($request);
    }

    public function logout(Request $request)
    {
        return $this->service->process($request);
    }
}
