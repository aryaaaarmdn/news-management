<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Post;

use App\Jobs\ProcessComment;
use Illuminate\Support\Facades\Log;

class CommentController extends Controller
{
    /**
     * Handle the incoming request.
     */
    // php artisan queue:work --queue=comments
    public function __invoke(StoreCommentRequest $request, Post $id)
    {
        $user = $request->user();
        
        try {
            ProcessComment::dispatch($request->content, $id, $user)->onQueue('comments');
        } catch (\Exception $e) {
            Log::error('Terjadi kesalahan saat mengirim pekerjaan ke antrian: ' . $e->getMessage());

            return response()->json(['error' => 'Terjadi kesalahan saat memproses komentar'], 500);
        }
        return response()->json([
            'status' => 200,
            'msg' => 'Komentar sedang diproses'
        ], 200);
    }
}
