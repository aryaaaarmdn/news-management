<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Http\Resources\PostResource;
use App\Events;
use App\Repositories\Post\PostRepositoryInterface;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $repository;

    public function __construct(PostRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $data = $this->repository->getAll();
        return PostResource::collection($data);
    }

    public function store(StoreRequest $request)
    {
        $data = $this->repository->store($request);

        if (!$data[0]) {
            return response()->json([
                'status' => 500,
                'msg' => $data[1],
            ], 500);
        }

        event(new Events\PostHasBeenManipulated('Menambahkan post baru', $request->user()));

        return new PostResource($data[1]);
    }

    public function update(UpdateRequest $request, Post $id)
    {
        $data = $this->repository->update($request, $id);

        if (!$data[0]) {
            return response()->json([
                'status' => 500,
                'msg' => $data[1],
            ], 500);
        }

        event(new Events\PostHasBeenManipulated('Memperbarui Post', $request->user()));

        return new PostResource($data[1]);
    }

    public function delete(Request $request, Post $id)
    {
        if (!$request->user()->tokenCan('delete-post')) {
            return response()->json([
                'status' => 403,
                'msg' => 'Unauthorized'
            ], 403);
        }

        event(new Events\PostHasBeenManipulated('Menghapus Post', $request->user()));

        return $this->repository->delete($id);
    }
}
