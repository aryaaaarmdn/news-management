<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'judul' => $this->title,
            'konten' => $this->content,
            'gambar' => new ImageResource($this->whenLoaded('image')),
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
            'waktu_dibuat' => $this->created_at->format('Y-m-d H:i:s'),
            'waktu_diperbarui' => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
