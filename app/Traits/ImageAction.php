<?php

namespace App\Traits;

use App\Exceptions\ImageException;

// Sewaktu waktu semisalkan untuk kedepannya ada case upload image tapi bukan untuk post
trait ImageAction
{
    public function upload($img)
    {
        if ($img->getError() != 0) {
            throw new ImageException('Terjadi Kesalahan Saat Upload Gambar');
        }
        $img->store($this->path);
    }

    public function delete($img)
    {
        if (!file_exists(public_path('storage/' . $img))) {
            throw new ImageException('Terjadi Kesalahan Saat Menghapus Gambar');
        }

        unlink(public_path('storage/' . $img));
    }
}