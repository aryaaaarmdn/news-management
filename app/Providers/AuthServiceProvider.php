<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

use App\Http\Controllers\AuthenticationController;
use App\Services\Authentication;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Passport::tokensCan([
            'create-post' => 'Create Post',
            'update-post' => 'Update Post',
            'delete-post' => 'Delete Post',
            'create-comment' => 'Create Comment',
        ]);
    }

    public function register()
    {
        // $this->app->when(AuthenticationController::class)
        //     ->needs(Authentication\TypeOfAuthenticationable::class)
        //     ->give(function () {
        //         $routeName = request()->route() ? 
        //             request()->route()->getName() : null ;
        //         if ($routeName == 'register') {
        //             return $this->app->make(Authentication\RegisterService::class);
        //         } elseif ($routeName == 'login') {
        //             return $this->app->make(Authentication\LoginService::class);
        //         } elseif ($routeName == 'logout') {
        //             return $this->app->make(Authentication\LogoutService::class);
        //         } else  {
        //             throw new \Exception(
        //                 'Sedikit bug karena route() tidak dikenal pada service provider. Mungkin karena service provider dijalankan sebelum http request. Sudah coba di file middleware terpisah tetap error ketika menjalankan perintah php artisan route:list. Tapi sudah dicoba semuanya sepertinya berjalan dengan baik. Next time dicoba cari cara untuk memperbaikinya'
        //             );
        //         }
        //     });
    }
}
