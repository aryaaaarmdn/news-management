<?php

namespace App\Providers;

use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Queue::before(function (JobProcessing $event) {
            Log::info('before connectionName: ' . $event->connectionName);
            Log::info('before getJobId: ' . $event->job->getJobId());
            Log::info('before getQueue: '. $event->job->getQueue());
            Log::info('before payload: ' . json_encode($event->job->payload()));
        });
    
        Queue::after(function (JobProcessed $event) {
            Log::info('after connectionName: ' . $event->connectionName);
            Log::info('after getJobId: ' . $event->job->getJobId());
            Log::info('after getQueue: '. $event->job->getQueue());
            Log::info('after payload: ' . json_encode($event->job->payload()));
        });
    }
}
