<?php

namespace App\Repositories\Post;

use App\Models\Image;
use App\Models\Post;
use App\Traits\ImageAction;

class PostRepository implements PostRepositoryInterface
{
    use ImageAction {
        delete as deleteImage;
    }

    private $path = 'posts/';

    public function getAll()
    {
        return Post::with(['image', 'comments', 'comments.user'])->paginate(1);
    }

    public function store($request)
    {
        $error = '';

        $this->upload($request->image);

        try {
            $post = Post::create([
                'title' => $request->title,
                'content' => $request->content
            ]);

            $image = Image::create([
                'path' => $this->path . $request->file('image')->hashName(),
                'name' => $request->file('image')->hashName(),
            ]);

            $post->image()->associate($image);
            $post->save();

        } catch (\Illuminate\Database\QueryException $exception) {
            $this->deleteImage($request->file('image')->hashName());

            $error = 'Terjadi kesalahan saat menambahkan data post';

            return [false, $error];
        }

        return [true, $post];
    }

    public function update($request, $model)
    {
        $error = '';

        if ($request->has('image')) {
            $this->deleteImage($model->image->path);
            $this->upload($request->image);
        }

        try {
            $model->update([
                'title' => $request->title,
                'content' => $request->content,
            ]);

            if ($request->has('image')) {
                $image = Image::findOrFail($model->image->id);
                $image->path = $this->path . $request->file('image')->hashName();
                $image->name = $request->file('image')->hashName();
                $image->save();
            }

            $model->load('image');

        } catch (\Illuminate\Database\QueryException $exception) {
            if ($request->has('image')) {
                $this->deleteImage($request->file('image')->hashName());
            }

            $error = 'Terjadi kesalahan saat memperbarui data post';
            return [false, $error];
        }

        return [true, $model];
    }

    public function delete($id)
    {
        $this->deleteImage($id->image->path);
        $image = Image::find($id->image->id);
        
        $id->image()->dissociate();
        $id->delete();
        $image->delete();

        return response()->json([
            'status' => 200,
            'msg' => 'Berhasil hapus post'
        ], 200);
    }
}