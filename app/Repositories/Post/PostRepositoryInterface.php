<?php

namespace App\Repositories\Post;

interface PostRepositoryInterface 
{
    public function getAll();
    public function store($request);
    public function update($request, $id);
    public function delete($id);
}