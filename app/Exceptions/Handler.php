<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (Throwable $e, $request) {
            if ($e instanceof NotFoundHttpException) {
                if ($e->getPrevious() instanceof ModelNotFoundException) {
                    return response()->json([
                        'msg' => 'Data tidak ditemukan',
                        'msg_detail' => $e->getMessage()
                    ], 404);
                } else {
                    return response()->json([
                        'status' => 500,
                        'msg' => 'Jangan lempar token pada endpoint yang dilindungi middleware guest',
                    ], 500);
                }
            } 
        });
    }
}
