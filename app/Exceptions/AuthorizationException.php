<?php

namespace App\Exceptions;

use Exception;

class AuthorizationException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => $this->getCode(),
            'msg' => $this->getMessage()
        ], $this->getCode());
    }
}
