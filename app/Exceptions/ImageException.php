<?php

namespace App\Exceptions;

use Exception;

class ImageException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => '',
            'msg' => $this->getMessage()
        ]);
    }
}
