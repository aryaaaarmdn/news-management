<?php

namespace App\Jobs;

use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessComment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user, $post, $content;

    /**
     * Create a new job instance.
     */
    public function __construct($content, $post, $user)
    {
        $this->content = $content;
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $comment = new Comment();
        $comment->content = $this->content;
        $comment->post()->associate($this->post);
        $comment->user()->associate($this->user);

        $comment->save();
    }
}
