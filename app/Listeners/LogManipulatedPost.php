<?php

namespace App\Listeners;

use App\Events\PostHasBeenManipulated;
use App\Models\PostLog;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class LogManipulatedPost
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(PostHasBeenManipulated $event): void
    {
        $post_log = new PostLog();
        $post_log->description = $event->desc;
        $post_log->user()->associate($event->user);
        $post_log->save();

        Log::info($event->desc . ' oleh: ' . $event->user);
    }
}
