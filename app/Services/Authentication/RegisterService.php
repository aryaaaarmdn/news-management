<?php

namespace App\Services\Authentication;

use App\Http\Resources\UserResource;
use App\Models\User;

use Exception;

class RegisterService implements TypeOfAuthenticationable
{
    public function process($request)
    {
        try {
            $user = User::create($request->all());
        } catch(Exception $e){
            return response()->json([
                'status' => 422,
                'msg' => 'Terjadi Kesalahan Saat Registrasi',
                'msg_detail' => $e->getMessage(),
            ], 422);
        }

        return (new UserResource($user))->additional([
            'msg' => 'Berhasil Registrasi',
            'status' => 200,
        ]);
    }
}