<?php

namespace App\Services\Authentication;

use App\Http\Resources\UserResource;
use App\Models\User;

use Illuminate\Support\Facades\Hash;

class LoginService implements TypeOfAuthenticationable
{
    public function process($request)
    {
        $user = User::where('email', $request->email)->first();
        $validate = $this->validate($user, $request);

        if (!$validate) {
            return $this->getErrorMsg();
        }

        $user->tokens->each(function ($token, $key) {
            $token->revoke();
        });

        $token = $this->generateToken($user);
        $user->withAccessToken($token);

        return (new UserResource($user))->additional([
            'msg' => 'Berhasil Login',
            'status' => 200,
        ]);
    }

    private function validate($user, $request)
    {
        if (!$user || !Hash::check($request->password, $user->password)) {
            return false;
        }
        return true;
    }

    private function getErrorMsg()
    {
        return response()->json([
            'status' => 401,
            'message' => 'Email / Password Salah',
        ], 401);
    }

    private function generateToken($user)
    {
        if ($user->is_admin === 1) {
            $token = $user->createToken('admin', [
                'create-post', 'update-post', 'delete-post'
            ])->accessToken;
        } else {
            $token = $user->createToken('non-admin', ['create-comment'])->accessToken;
        }
        return $token;
    }
}