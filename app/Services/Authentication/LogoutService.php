<?php

namespace App\Services\Authentication;

class LogoutService implements TypeOfAuthenticationable
{
    public function process($request)
    {
        $request->user()->token()->delete();
        return response()->noContent();
    }
}