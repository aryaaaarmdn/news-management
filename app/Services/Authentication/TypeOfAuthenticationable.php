<?php

namespace App\Services\Authentication;

interface TypeOfAuthenticationable
{
    public function process($request);
}