<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware(['guest:api'])->group(function() {
    Route::controller(AuthenticationController::class)->group(function () {
        Route::post('/register', 'register')->name('register');
        Route::post('/login', 'login')->name('login');
    });
});

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [AuthenticationController::class, 'logout'])
        ->name('logout');

    Route::controller(PostController::class)->group(function (){
        Route::get('/post', 'getAll');
        Route::post('/post/store', 'store');
        Route::put('/post/{id}/update', 'update');
        Route::delete('/post/{id}/delete', 'delete');
    }); 

    Route::post('/comment/post/{id}', CommentController::class);
});


